package j1017.controller;

import java.sql.Date;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import j1017.model.Material;
import j1017.model.Robot;
import j1017.repository.MaterialJPARepository;
import j1017.repository.RobotJPARepository;

@Controller 
public class DataBaseCreateController {
    @Autowired
    RobotJPARepository robotJPARepository;
    
    @Autowired
    MaterialJPARepository materialJPARepository;
    
    @ResponseBody
	@RequestMapping("/create")
	public String create() {
		Material wood = new Material("wood");
		Material concrete = new Material("concrete");
		materialJPARepository.saveAll(Arrays.asList(wood,concrete));
		
		Robot robot1 = new Robot("Tom", "20", new Date(System.currentTimeMillis()),wood);
		Robot robot2 = new Robot("Mumu", "30", new Date(System.currentTimeMillis()),concrete);
		Robot robot3 = new Robot("Gerasim", "35", new Date(System.currentTimeMillis()),wood);
		robotJPARepository.saveAll(Arrays.asList(robot1, robot2, robot3));
		
		return "Complete";
	}
}
