package j1017.controller;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import j1017.model.Material;
import j1017.model.Robot;
import j1017.repository.MaterialJPARepository;
import j1017.repository.RobotJPARepository;


@RestController
public class ControllerRobot {
	@Autowired
	private Logger logger;
	@Autowired
	private MaterialJPARepository materialJPARepository;

	@Autowired
	private RobotJPARepository repository;

	@RequestMapping("/robot/{materialname}")
	public String index(@RequestParam(required = false) String name,
		@PathVariable(required = false,value ="materialname") String materialname) {
		logger.trace("We're in robot with name " + name+" Path Variable "+materialname);
		Optional<Material> material = materialJPARepository.findByValue(materialname);
		logger.trace("Material found by name: "+material);
		String message;
		if(material.isPresent()) {
			Set<Robot> robotSet = repository.findByMaterial(material.get());
			if (!robotSet.isEmpty()) {
				
				message = "Robots made from "+materialname +" : "+ robotSet;
			} else {
				message = "Robots made from "+materialname+" not found";
			}
		}else {
			message = "Material with name: "+materialname+" not found";
		}
//		Optional<Material> materialOptional = materialJPARepository.findById((long)4);
		//List <Robot> robot = repository.findAll();
		

		return message;
	}
}
