package j1017.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import j1017.model.Material;

public interface MaterialJPARepository extends JpaRepository<Material, Long> {

	public Optional<Material> findByValue(String value);
}
