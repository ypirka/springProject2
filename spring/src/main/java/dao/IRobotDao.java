package dao;

import java.util.List;
import j1017.model.Robot;

public interface IRobotDao {
	List<Robot> getAllRobots();

	Robot getRobotById(Long robotId);

	void addRobot(Robot robot);

	void updateRobot(Robot robot);

	void deleteRobot(Long robotId);
}
